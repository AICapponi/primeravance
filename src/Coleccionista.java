import java.awt.image.BufferedImage;
import java.util.Date;

/**
 * Clase para la manipulacion de datos de los coleccionistas
 *
 * @author Alejandro Capponi
 */
public class Coleccionista extends Usuario{

    private BufferedImage avatar;
    private int identificacion;
    private String fechaNacimiento;
    private int edad;
    private Boolean estado;
    private String direccion;
    private int puntuacion;
    private String intereses;
    private String objetos;
    private Boolean esModerador;
    private String contrasena;

    /**
     * Metodo constructor por defecto
     */
    public Coleccionista() {

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombreCompleto Nombre completo del usuario coleccionista, heredado de la clase Usuario
     * @param correoElectronico Correo Electronico del usuario coleccionista, heredado de la clase Usuario
     * @param avatar Avatar o imagen de perfil del usuario coleccionista
     * @param identificacion Identificacion del usuario coleccionista
     * @param fechaNacimiento Fecha de nacimiento del usuario coleccionista
     * @param edad edad del usuario coleccionista, calculada en base a la fecha de nacimiento
     * @param estado Estado del usuario coleccionista, si es activo o inactivo (Booleano, true para activo y false para inactivo)
     * @param direccion Direccion de residencia del usuario coleccionista
     * @param puntuacion Puntuacion promedio que tiene el usuario coleccionista dentro de la aplicacion
     * @param intereses Intereses que tiene el usuario coleccionista y lista dentro de la aplicacion, intereses de compra
     * @param objetos Serie de objetos de los que el coleccionista es duenio
     * @param esModerador Variable de tipo booleano para especificar si un usuario coleccionista es un moderador tambien
     * @param contrasena Clave para el log in del usuario coleccionista
     */
    public Coleccionista(String nombreCompleto, String correoElectronico, BufferedImage avatar, int identificacion, String fechaNacimiento, int edad, Boolean estado, String direccion, int puntuacion, String intereses, String objetos, Boolean esModerador, String contrasena) {
        super(nombreCompleto, correoElectronico);
        this.avatar = avatar;
        this.identificacion = identificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.estado = estado;
        this.direccion = direccion;
        this.puntuacion = puntuacion;
        this.intereses = intereses;
        this.objetos = objetos;
        this.esModerador = esModerador;
        this.contrasena = contrasena;
    }

    public BufferedImage getAvatar() {
        return avatar;
    }

    public void setAvatar(BufferedImage avatar) {
        this.avatar = avatar;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public String getIntereses() {
        return intereses;
    }

    public void setIntereses(String intereses) {
        this.intereses = intereses;
    }

    public String getObjetos() {
        return objetos;
    }

    public void setObjetos(String objetos) {
        this.objetos = objetos;
    }

    public Boolean getEsModerador() {
        return esModerador;
    }

    public void setEsModerador(Boolean esModerador) {
        this.esModerador = esModerador;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Coleccionista{" +
                "avatar=" + avatar +
                ", identificacion=" + identificacion +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", estado=" + estado +
                ", direccion='" + direccion + '\'' +
                ", puntuacion=" + puntuacion +
                ", intereses='" + intereses + '\'' +
                ", objetos='" + objetos + '\'' +
                ", esModerador=" + esModerador +
                ", contrasena='" + contrasena + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", correoElectronico='" + correoElectronico + '\'' +
                '}';
    }
}
