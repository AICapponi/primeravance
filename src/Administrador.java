import java.awt.image.BufferedImage;
import java.util.Date;


/**
 * Clase para la manipulacion de Administrador
 *
 * @author Alejandro Capponi
 */
public class Administrador extends Usuario{

    private BufferedImage avatar;
    private int identificacion;
    private String fechaNacimiento;
    private int edad;
    private Boolean estado;
    private String contrasena;

    /**
     * Metodo constructor por defecto
     */
    public Administrador(){

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombreCompleto Nombre completo del administrador, heredado de la clase Usuario
     * @param correoElectronico Correo Electronico del administrador, heredado de la clase Usuario
     * @param avatar Imagen de avatar de perfil del administrador
     * @param identificacion Identificacion del administrador
     * @param fechaNacimiento Fecha de nacimiento del administrador
     * @param edad Edad del administrador, calculada en base a fecha de nacimiento
     * @param estado Estado del administrador, si es activo o inactivo (De tipo booleano, true siendo activo y false siendo inactivo)
     * @param contrasena (Clave para el login del administrador)
     */
    public Administrador(String nombreCompleto, String correoElectronico, BufferedImage avatar, int identificacion, String fechaNacimiento, int edad, Boolean estado, String contrasena) {
        super(nombreCompleto, correoElectronico);
        this.avatar = avatar;
        this.identificacion = identificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.estado = estado;
        this.contrasena = contrasena;
    }

    public BufferedImage getAvatar() {
        return avatar;
    }

    public void setAvatar(BufferedImage avatar) {
        this.avatar = avatar;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Administrador{" +
                "avatar=" + avatar +
                ", identificacion=" + identificacion +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", estado=" + estado +
                ", contrasena='" + contrasena + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", correoElectronico='" + correoElectronico + '\'' +
                '}';
    }
}
