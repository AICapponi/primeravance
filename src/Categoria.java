/**
 * Clase para la manipulacion de categorias de los items
 *
 * @author Alejandro Capponi
 */

public class Categoria {

    private String nombre;
    private int codigo;
    private Boolean estado;

    /**
     * Metodo constructor por defecto
     */
    public Categoria(){

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombre Nombre de la categoria
     * @param codigo Codigo numerico autogenerado por aplicacion para cada categoria
     * @param estado Estado de si la categoria esta activa o inactiva (Booleano, true si es activo y false si es inactivo)
     */
    public Categoria(String nombre, int codigo, Boolean estado) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Categoria{" +
                "nombre='" + nombre + '\'' +
                ", codigo=" + codigo +
                ", estado=" + estado +
                '}';
    }
}
