import java.awt.image.BufferedImage;
import java.util.Date;

/**
 * Clase para la manipulacion de datos de los items
 *
 * @author Alejandro Capponi
 */
public class Item {

    private String nombre;
    private String descripcion;
    private Estado estado;
    private BufferedImage imagenes;
    private String fechaCompra;
    private int Antiguedad;
    private Categoria categoria;

    /**
     * Metodo constructor por defecto
     */
    public Item(){

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombre Nombre del item especifico
     * @param descripcion Descripcion del item
     * @param estado Estado en el que se encuentra el item
     * @param imagenes Imagenes del item
     * @param fechaCompra Fecha en el que el item fue comprado
     * @param antiguedad Antiguedad del item, basado y calculado en la fecha de compra
     * @param categoria La categoria en la cual cae el item
     */
    public Item(String nombre, String descripcion, Estado estado, BufferedImage imagenes, String fechaCompra, int antiguedad, Categoria categoria) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.imagenes = imagenes;
        this.fechaCompra = fechaCompra;
        Antiguedad = antiguedad;
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public BufferedImage getImagenes() {
        return imagenes;
    }

    public void setImagenes(BufferedImage imagenes) {
        this.imagenes = imagenes;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public int getAntiguedad() {
        return Antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        Antiguedad = antiguedad;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Item{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", estado=" + estado +
                ", imagenes=" + imagenes +
                ", fechaCompra=" + fechaCompra +
                ", Antiguedad=" + Antiguedad +
                ", categoria=" + categoria +
                '}';
    }
}
