import java.io.*;
import java.util.ArrayList;

/**
 * Clase donde se ejecuta el main, se solicitan los datos para luego ser procesados en otra clase
 * @author Alejandro Capponi
 */

public class UI {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    /**
     * Metodo para mostrar las opciones del menu
     */
    static void mostrarMenu(){
        System.out.println("1. Registrar Cliente");
        System.out.println("2. Registrar Cuenta");
        System.out.println("3. Listar Clientes");
        System.out.println("4. Realizar Deposito");
        System.out.println("5. Realizar Retiro");
        System.out.println("6. Mostrar saldo de cuenta");
        System.out.println("0. Salir");
    }

    /**
     * Metodo para leer la opcion del menu que se quiera procesar
     * @return Retorna la opcion leida que se ha digitado
     * @throws IOException
     */
    static int seleccionarOpcion() throws IOException{
        System.out.println("Digite la opcion");
        return Integer.parseInt(in.readLine());
    }

    /**
     * Metodo que procesa la opcion digitada para proceder de alguna manera especifica
     * @param pOpcion Opcion seleccionada del menu
     * @throws IOException
     */
    static void procesarOpcion(int pOpcion) throws IOException{
        switch (pOpcion) {
            case 1:

                break;
            case 2:

                break;
            case 3:

                break;
            case 4:

                break;
            case 5:

                break;
            case 6:

                break;
            case 0:
                System.out.println("Gracias por usar el programa");
                break;
            default:
                System.out.println("Opcion invalida, digite una opcion entre el 0 y * dependiendo de lo que desee hacer en el programa");
                break;
        }
    }

    public static void main (String[] args) throws IOException{
        int opcion = -1;
        do {
            mostrarMenu();
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);

        } while (opcion !=0);

    }

}
