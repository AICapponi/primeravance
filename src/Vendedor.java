/**
 * Clase para la manipulacion de datos del usuario vendedor
 *
 * @author Alejandro Capponi
 */

public class Vendedor extends Usuario{

    private String direccion;

    /**
     * Metodo constructor por defecto
     */
    public Vendedor() {

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombreCompleto Nombre completo del usuario vendedor, heredado de la clase Usuario
     * @param correoElectronico Correo Electronico del usuario vendedor, heredado de la clase Usuario
     * @param direccion Direccion de residencia del usuario vendedor
     */
    public Vendedor(String nombreCompleto, String correoElectronico, String direccion) {
        super(nombreCompleto, correoElectronico);
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Vendedor{" +
                "nombreCompleto='" + nombreCompleto + '\'' +
                ", correoElectronico='" + correoElectronico + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
}
