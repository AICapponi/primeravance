import java.util.Date;

/**
 * Clase para la manipulacion de datos de las subastas
 *
 * @author Alejandro Capponi
 */
public class Subasta {

    private Item item;
    private Date fechaCreacion;
    private Date fechaInicio;
    private String fechaVencimiento;
    private Double precioMinimo;
    private String nombreVendedor;
    private Double oferta;
    private Double mayorOferta;
    private String ganador;

    /**
     * Metodo constructor por defecto
     */
    public Subasta(){

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param item Item o items que se estan subastando
     * @param fechaCreacion Fecha de creacion de la subasta
     * @param fechaInicio Fecha en la que inica la subasta y se abre al publico
     * @param fechaVencimiento Fecha de vencimiento de la subasta, cuando va a cerrar
     * @param precioMinimo El precio minimo en el que inicia la subasta, lo minimo que se acepta por el o los items
     * @param nombreVendedor Nombre del vendedor de el o los items, ya sea un usuario vendedor o un usuario coleccionista
     * @param oferta Ofertas que se hacen por el o los items
     * @param mayorOferta La mayor oferta que haya para cuando finalice la subasta, esto determina el ganador de la subasta
     * @param ganador Identifica quien hizo la mayor oferta y determina que ese es el ganador, guarda sus datos
     */
    public Subasta(Item item, Date fechaCreacion, Date fechaInicio, String fechaVencimiento, Double precioMinimo, String nombreVendedor, Double oferta, Double mayorOferta, String ganador) {
        this.item = item;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        this.fechaVencimiento = fechaVencimiento;
        this.precioMinimo = precioMinimo;
        this.nombreVendedor = nombreVendedor;
        this.oferta = oferta;
        this.mayorOferta = mayorOferta;
        this.ganador = ganador;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Double getPrecioMinimo() {
        return precioMinimo;
    }

    public void setPrecioMinimo(Double precioMinimo) {
        this.precioMinimo = precioMinimo;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public Double getOferta() {
        return oferta;
    }

    public void setOferta(Double oferta) {
        this.oferta = oferta;
    }

    public Double getMayorOferta() {
        return mayorOferta;
    }

    public void setMayorOferta(Double mayorOferta) {
        this.mayorOferta = mayorOferta;
    }

    public String getGanador() {
        return ganador;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Subasta{" +
                "item=" + item +
                ", fechaCreacion=" + fechaCreacion +
                ", fechaInicio=" + fechaInicio +
                ", fechaVencimiento=" + fechaVencimiento +
                ", precioMinimo=" + precioMinimo +
                ", nombreVendedor='" + nombreVendedor + '\'' +
                ", oferta=" + oferta +
                ", mayorOferta=" + mayorOferta +
                ", ganador='" + ganador + '\'' +
                '}';
    }
}
