/**
 * Clase para la manipulacion de datos del estado de los items
 *
 * @author Alejandro Capponi
 */
public class Estado {

    private String nombre;
    private Boolean estado;
    private int codigo;

    /**
     * Metodo constructor por defecto
     */
    public Estado(){

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombre Nombre del item al cual se le agrega el estado
     * @param estado El estado del item, si es nuevo o usado (Booleano, true para nuevo y false para usado)
     * @param codigo Codigo numerico autogenerado por la aplicacion
     */
    public Estado(String nombre, Boolean estado, int codigo) {
        this.nombre = nombre;
        this.estado = estado;
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Estado{" +
                "nombre='" + nombre + '\'' +
                ", estado=" + estado +
                ", codigo=" + codigo +
                '}';
    }
}
