/**
 * Clase principal de donde los demas usuarios heredan los datos del nombre y correo electronico
 *
 * @author Alejandro Capponi
 */

public class Usuario {

   protected String nombreCompleto;
   protected String correoElectronico;

    /**
     * Metodo constructor por defecto
     */
    public Usuario() {

    }

    /**
     * Metodo constructor para asignar los valores a las variables
     * @param nombreCompleto Nombre completo del usuario
     * @param correoElectronico Correo Electronico del usuario
     */
    public Usuario(String nombreCompleto, String correoElectronico) {
        this.nombreCompleto = nombreCompleto;
        this.correoElectronico = correoElectronico;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    /**
     * Metodo para devolver el String de los datos
     * @return El string de la clase y sus datos
     */
    @Override
    public String toString() {
        return "Usuario{" +
                "nombreCompleto='" + nombreCompleto + '\'' +
                ", correoElectronico='" + correoElectronico + '\'' +
                '}';
    }
}
